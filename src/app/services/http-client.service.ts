import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(private httpClient : HttpClient) { 
  }

  get(URL){
    console.log("GET: ", URL)
    return this.httpClient.get(URL);
  }

  post(DATA:any){
    console.log("POST: ", DATA) 
    let url = DATA.Url;
    let data = (DATA.Data); 
    return this.httpClient.post(url,data);  
  }

}
