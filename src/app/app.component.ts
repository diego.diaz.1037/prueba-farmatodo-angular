import { Component, OnInit } from '@angular/core';
import { HttpClientService } from './services/http-client.service'
import {MatDialog, MatDialogRef,} from '@angular/material/dialog';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {

  public URLzip : any = 'https://blackisp.herokuapp.com/postalCodes/';
  public URLdata : any = 'https://blackisp.herokuapp.com/contact';
  public URLproducts : any = 'https://blackisp.herokuapp.com/products';

  public colonies:any[]; 

  public DirectionData: any = {
    name : "",
    lastname : "",
    email : "",
    phone : "",
    postal_number : "",
    colony : "",
    state : "",
    city : "",
    town : "",
    street: "",
  }

  public products:any[];
  public images:any[] = [];
  public names:any[] = [];
  public prices:any[] = [];
  
  checked = false;
  indeterminate = false;
  labelPosition: 'before' | 'after' = 'after';
  disabled = false;

  constructor(
    private HttpClient:HttpClientService,
    public dialog: MatDialog) {

      this.HttpClient.get(this.URLproducts).subscribe(
        res=>{
          console.log(res)
          let success:any = (res)
          this.products = success;
          console.log(this.products)

          for ( let i=0; i< this.products.length;i++){
            let image = this.products[i].image;
            this.images.push(image);
            }
            console.log("PRODUCTS images: ", this.images)

            for ( let i=0; i< this.products.length;i++){
              let name  = this.products[i].name;
              this.names.push(name);
              }
              console.log("PRODUCTS name: ", this.names)

              for ( let i=0; i< this.products.length;i++){
                let price = this.products[i].price;
                this.prices.push(price);
                }
                console.log("PRODUCTS prices: ", this.prices)
       
        },
        err=> console.log(err)
      )
      
  }

  ngOnInit() {}

  ZipCode(){
    console.log(this.DirectionData.postal_number)
    let url = this.URLzip + this.DirectionData.postal_number
    this.HttpClient.get(url).subscribe(
      res=>{
        console.log(res)
        let success:any = (res)
        this.colonies = success.colonies;
        this.DirectionData.state = success.state;
        this.DirectionData.city = success.city;
        this.DirectionData.town= success.town;

      },
      err=> console.log(err)
    )
    
  }

  Save(){
    console.log(this.DirectionData)

    this.HttpClient.post({Url:this.URLdata, Data:this.DirectionData}).subscribe(
      res=>{
      console.log("SUCCESS", res)
      this.dialog.open(DialogSuccess);
      },
      err=> console.log("ERROR", err)
    )
  }

}



@Component({
  selector: 'DialogSuccess',
  templateUrl: './components/DialogSuccess.html',
})
export class DialogSuccess
{

  constructor(
    public dialogRef: MatDialogRef<DialogSuccess>,
    ) {}


  Close(): void {
    this.dialogRef.close();
  }
}